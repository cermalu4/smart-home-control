const fs = require('fs')

const HomeService = require('./HomeService')

function getProfiles() {
  try {
    const home = HomeService.getHome();
    return home.profiles
  } catch (e) {
    throw `Couldn't load profiles ${e}.`
  }

}

function getProfile(profileId) {
  try {
    const profiles = getProfiles();
    const profile = profiles[profileId];
    if (profile) {
      return profile
    } else {
      throw `Profile doesn't exist.`
    }
  } catch (e) {
    throw `Couldn't get profile ${profileId}: ${e}`
  }
}

function checkProfile(credentials) {
  try {
    const profile = getProfile(credentials.profileId);
    if (profile) {
      return profile.pin === credentials.profilePin
    } else {
      throw `Profile with id ${credentials.profileId} doesn't exist.`
    }
  } catch (e) {
    throw `Couldn't check profile: ${e}`
  }
}

module.exports = {
  signIn: function(credentials) {
    try {
      if (checkProfile(credentials)) {
        let profile = getProfile(credentials.profileId);
        profile.isSigned = true;
        //TODO control that nobody is signed, if yes, then set isSigned false
        const home = HomeService.getHome()
        home.profiles[credentials.profileId] = profile;
        HomeService.setHome(home);
        const {pin, ...result} = profile
        result.profileId = credentials.profileId;
        return result;
      } else {
        throw 'Pin missmatch!'
      }
    } catch (e) {
      throw e
    }
  },

  signOut: function(profileId) {
    try {
      const pr = getProfile(profileId)
      console.log(pr)
      const home = HomeService.getHome()
      let profile = home.profiles[profileId];
      profile.isSigned = false;
      HomeService.setHome(home)
    } catch (e) {
      throw e
    }
  },

  getProfiles
}
