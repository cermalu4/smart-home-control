const fs = require('fs')
const Response = require('../libs/Response')
const Utils = require('../libs/Utils')
const ProfileService = require('../services/ProfileService')

module.exports = {
  getProfiles: function(req, res) {
    const profiles = ProfileService.getProfiles()
    try {
      Response.success(
        res,
        profiles
      )
    } catch (error) {
      Response.error(res, error.message)
    }
  },

  signIn: function(req, res) {
    const profileId = req.body.id
    const profilePin = req.body.pin

    try {
      const profile = ProfileService.signIn({profileId, profilePin});
      Response.success(res, {
        profile
      })
    } catch (e) {
      Response.error(res, e, 400)
    }
  },

  signOut: function(req, res) {
    const profileId = req.body.id
    try {
      ProfileService.signOut(profileId)
      Response.success(res, "Successfully signed out.")
    } catch (e) {
      Response.error(res, e, 400)
    }
  }
}
