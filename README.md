# Smart Home Control

CTU project for a course Management of Software Projects (B6B36RSP)

It's an application for **SMART TVs**, which offers control of smart homes.

## Fuctionality
- [x] Get Home config
- [x] Get consumption of the home
- [x] Get profiles of the home
- [x] Sign in to profile
- [x] Sign out
- [x] Get actual temperature of the home
- [x] Set home temperature
- [x] Get weather
- [x] Get CCTV footage

### Electricity Consumption
For simplication on server side, we represent consumption of each device as **kWh per day**.

## Build & Run

#### install dependencies
```
npm install
```

#### compiles and hot-reloads for development
```
npm run dev
```

#### compiles and runs the server
```
npm run start
```
## Server

Server is running on NodeJS and offers lightweight API.

There is no database maintained, **all configuration and data of a home** will be in a JSON file.

`/server/db/Home.json`

In a development mode a server runs at http://127.0.0.1:3000

### API

API offers multiple actions, which can be called to get some data from JSON file, add some data or make changes.

Response body will always look like this:
```
{
  "state": // "success" or "error",
  "data": // Some data
}
```

#### Endpoints

1. Get a whole home object
    - type: GET
    - route: **/**
    - returns: object with state and home object

2. Get consumption of a whole home, based on given date range
    - type: GET
    - route: **/consumption**
    - returns: object with state and total consumption number
    - request body: date range object (format: YYYY-MM-DD)
    ```
    // example of an date range object
    {
	    "from": "2019-01-01",
	    "to": "2019-03-30"
    }
    ```
3. Get home profiles
    - type: GET
    - route: **/profiles**
    - returns: object with state and object with profiles
    - request body: none

4. Sign In
    - type: POST
    - route: **/signin**
    - returns: object with state and message
    - request body: object with profile id and input pin
    ```
    // example of the request object
    {
	    "id": "profile_1",
	    "pin": "1111"
    }
    ```
5. Sign Out
    - type: POST
    - route: **/signout**
    - returns: object with state and message
    - request body: object with profile id
    ```
    // example of the request object
    {
	    "id": "profile_1"
    }
    ```

6. Get CCTV footage
    - type: GET
    - route: **/cctv/{id}/footage**
    - returns: object with state and link to the footage
    - request body: none

7. Get actual temperature
    - type: GET
    - route: **/inhousetemp**
    - returns: object with state and temperature
    - request body: none

8. Set home temperature
    - type: GET
    - route: **/changetemp**
    - returns: object with state and message
    - request body: object with profile id
    ```
    // example of an request object
    {
	    "temp": 20
    }
    ```

9. Get actual weather
    - type: GET
    - route: **/getweather**
    - returns: object with state and weather
    - request body: none
