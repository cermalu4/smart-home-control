const fs = require('fs')
const Response = require('../libs/Response')
const HomeService = require('../services/HomeService')
const Utils = require('../libs/Utils')
var temp = 17.5, actualTemp = temp;

module.exports = {
  getHome: function(req, res) {
    try {
      Response.success(
        res,
        HomeService.getHome()
      )
    } catch (error) {
      Response.error(res, error.message)
    }
  },

  //Logic to service
  getWeather: function(req, res) {
    let data = [], date = new Date();
    let images = ['s', "storm", "rain"];
    for(let i = 0; i < 7; i++){

      data.push({
        date: new Date(date),
        image: "image/"+images[i%3]+".png",
        temp: '1'+i+'°C',
      });
      date.setDate(date.getDate()+1);
    }
    Response.success(res, data);
  },

  //Logic to service
  consumption: function(req, res) {
    const dateRange = req.query;
    const from = new Date(dateRange.from);
    const to = new Date(dateRange.to);
    const timeDiff = Math.abs(to.getTime() - from.getTime());
    const dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1; // +1 to include last day too

    try {
      const home = HomeService.getHome()
      let devices = [];
      // using THE WORST way...getting all devices of a home
      home.floors.forEach(floor => {
        floor.rooms.forEach(room => {
          room.devices.forEach(device => devices.push(device))
        })
      });

      let totalConsumption = Utils.computeConsumption(devices, dayDifference);
      if (totalConsumption !== null) {
        Response.success(res, totalConsumption)
      }

    } catch (error) {
      Response.error(res, error.message)
    }
  },

  //Logic to service
  inHouseTemp: function(req, res) {
    try {
      if(temp < actualTemp)
        actualTemp-=0.5;
      if(temp > actualTemp)
        actualTemp+=0.5;
      Response.success(
          res,
          actualTemp
      )
    } catch (error) {
      Response.error(res, error.message)
    }
  },

  setHouseTemp: function (req, res){
    temp = req.query.temp;
    Response.success(res, "Temp set");
  },

  getCctvFootage: function (req, res) {
    let tmpFootage = [
        "https://drive.google.com/file/d/1ZpW1lNY8Cgiwa-EOuft2FqKH5T4tlrfB/preview",
        "https://drive.google.com/file/d/1HhC_NuXQ9AI6spwcdaLX7RFjZuRzPLSq/preview",
        "https://drive.google.com/file/d/1Q5ZrWnsPkqFlBt-Ab8QccjcVd3Gu52Ch/preview"
    ];

    // Get Id of CCTV
    let cctvId = parseInt(req.params.id);

    // Try to parse the parameter with id
    if (isNaN(cctvId)) {
      Response.error(res, "Invalid id format", 400)
    }

    // Check if there is CCTV with this id
    if (cctvId < 0 || cctvId > tmpFootage.length - 1) {
      Response.error(res, "There is no CCTV with id " + cctvId, 400)
    } else {
      Response.success(res, tmpFootage[cctvId]);
    }
  }
}
