const fs = require('fs')
const path = require('path');

module.exports = {
  getHome: function() {
    try {
      return JSON.parse(fs.readFileSync(process.env.DEV_DB, 'utf-8'))
    } catch (e) {
      throw e
    }
  },

  setHome: function(home) {
    fs.writeFileSync(process.env.DEV_DB, JSON.stringify(home, null, 2), 'utf-8');
  }
}
